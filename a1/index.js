console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function showNames() {
	 		let functionVar = prompt("What is your name?");
	 		let functionConst = prompt("How old are you?");
			let functionLet = prompt("Where do you live?");

	 		console.log("Hello," + functionVar);
			console.log("You are " + functionConst + " years old.");
	 		console.log("You live in " + functionLet);
	 		alert("Thank you for your input!");
			}

			showNames();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

		function favBands() {
			var favBands1 = "Les Claypool";
			const favBands2 = "Metallica";
			let favBands3 = "Paramore";
			let favBands4 = "Maroon 5";
			let favBands5 = "Keane";

			console.log("1. " + favBands1);
			console.log("2. " + favBands2);
			console.log("3. " + favBands3);
			console.log("4. " + favBands4);
			console.log("5. " + favBands5);
		};
			favBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favMovies() {
		let favMovies1 = "About Time";
		console.log("1. " + favMovies1);
		console.log("Rotten Tomatoes Rating: 70%");

		let favMovies2 = "Jurassic World Dominion";
		console.log("2. " + favMovies2);
		console.log("Rotten Tomatoes Rating: 29%");

		let favMovies3 = "Doctor Strange in the Multiverse of Madness";
		console.log("3. " + favMovies3);
		console.log("Rotten Tomatoes Rating: 74%");

		let favMovies4 = "The Hunger Games";
		console.log("4. " + favMovies4);
		console.log("Rotten Tomatoes Rating: 84%");

		let favMovies5 = "Interstellar";
		console.log("5. " + favMovies5);
		console.log("Rotten Tomatoes Rating: 73%");
	};

		favMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printFriends() = function printUsers(){

	function friends() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 

	};

	friends();


// console.log(friend1);
// console.log(friend2);